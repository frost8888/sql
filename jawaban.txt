1.	//menjalankan mysql pada terminal
	C:\xampp\mysql>bin>mysql -uroot
	
	membuat databases
	MariaDB [(none)]> create databases myshop
	
	//menunjukan daftar databases
	MariaDB [(none)]> show databases
		-> ;
		
	//menggunakan databases
	MariaDB [(none)]> use myshop
	MariaDB [myshop]>
	

2.	//membuat table users
	MariaDB [myshop]> create table users(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
	
	//menampilkan table
	MariaDB [myshop]> describe users;
	+----------+--------------+------+-----+---------+----------------+
	| Field    | Type         | Null | Key | Default | Extra          |
	+----------+--------------+------+-----+---------+----------------+
	| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name     | varchar(255) | YES  |     | NULL    |                |
	| email    | varchar(255) | YES  |     | NULL    |                |
	| password | varchar(255) | YES  |     | NULL    |                |
	+----------+--------------+------+-----+---------+----------------+
	4 rows in set (0.026 sec)
	
	
	//membuat table categories
	MariaDB [myshop]> create table categories(
    -> id int primary key auto_increment,
    -> name varchar(255)
    -> );
	Query OK, 0 rows affected (0.057 sec)
	
	//menampilkan table
	MariaDB [myshop]> describe categories;
	+-------+--------------+------+-----+---------+----------------+
	| Field | Type         | Null | Key | Default | Extra          |
	+-------+--------------+------+-----+---------+----------------+
	| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name  | varchar(255) | YES  |     | NULL    |                |
	+-------+--------------+------+-----+---------+----------------+
	2 rows in set (0.022 sec)
	
	//membuat table items
	MariaDB [myshop]> create table items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> foreign key categories(id) references categories(id)
    -> );
	Query OK, 0 rows affected (0.144 sec)
	
	//menampilkan table
	MariaDB [myshop]> describe items;
	+-------------+--------------+------+-----+---------+----------------+
	| Field       | Type         | Null | Key | Default | Extra          |
	+-------------+--------------+------+-----+---------+----------------+
	| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name        | varchar(255) | YES  |     | NULL    |                |
	| description | varchar(255) | YES  |     | NULL    |                |
	| price       | int(11)      | YES  |     | NULL    |                |
	| stock       | int(11)      | YES  |     | NULL    |                |
	+-------------+--------------+------+-----+---------+----------------+
	5 rows in set (0.034 sec)
	
	//menambahkan kolom baru pada table
	MariaDB [myshop]> alter table items add category_id int;
	
	//menambahkan foreign key pada category_id
	MariaDB [myshop]> alter table items
    -> add foreign key(category_id) references categories(id)
    -> ;
	
	//menampilkan table
	MariaDB [myshop]> describe items;
	+-------------+--------------+------+-----+---------+----------------+
	| Field       | Type         | Null | Key | Default | Extra          |
	+-------------+--------------+------+-----+---------+----------------+
	| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name        | varchar(255) | YES  |     | NULL    |                |
	| description | varchar(255) | YES  |     | NULL    |                |
	| price       | int(11)      | YES  |     | NULL    |                |
	| stock       | int(11)      | YES  |     | NULL    |                |
	| category_id | int(11)      | YES  | MUL | NULL    |                |
	+-------------+--------------+------+-----+---------+----------------+
	6 rows in set (0.036 sec)
	
	
3.	//memasukan data 1
	MariaDB [myshop]> insert into users(name,email,password) values(
    -> "john doe","john@doe.com","john123");
	Query OK, 1 row affected (0.014 sec)
	
	//memasukan data 2
	MariaDB [myshop]> insert into users(name,email,password) values(
		-> "jane doe","jane@doe.com","jenita123");
	Query OK, 1 row affected (0.002 sec)
	
	//menampilkan data yang telah dimasukan
	MariaDB [myshop]> select * from users;
	+----+----------+--------------+-----------+
	| id | name     | email        | password  |
	+----+----------+--------------+-----------+
	|  1 | john doe | john@doe.com | john123   |
	|  2 | jane doe | jane@doe.com | jenita123 |
	+----+----------+--------------+-----------+
	2 rows in set (0.001 sec)
	
	//menambahkan data 
	MariaDB [myshop]> insert into categories(name) values('gadget'),('cloth'),('men'),('women'),('branded');
	Query OK, 5 rows affected (0.004 sec)
	Records: 5  Duplicates: 0  Warnings: 0
	
	//menampilkan data yang telah dimasukan
	MariaDB [myshop]> select * from categories;
	+----+---------+
	| id | name    |
	+----+---------+
	|  1 | gadget  |
	|  2 | cloth   |
	|  3 | men     |
	|  4 | women   |
	|  5 | branded |
	+----+---------+
	5 rows in set (0.000 sec
	
	// menambahkan data 1
	MariaDB [myshop]> insert into items(name,description,price,stock,category_id) values(
    -> "sumsang b50", "hape keren dari merek sumsang",4000000,100,1);
	Query OK, 1 row affected (0.007 sec)
	
	//menambahkan data 2
	MariaDB [myshop]> insert into items(name,description,price,stock,category_id) values(
		-> "uniklooh", "baju keren dari brand ternam",500000,50,2);
	Query OK, 1 row affected (0.002 sec)
	//menambahkan data 3
	MariaDB [myshop]> insert into items(name,description,price,stock,category_id) values(
		-> "imho watch", "jam tangan anak yang jujur banget",2000000,10,1)
		-> ;
	Query OK, 1 row affected (0.003 sec)
	
	//menampilkan hasil data yang telah dimasukan
	MariaDB [myshop]> select * from items;
	+----+-------------+-----------------------------------+---------+-------+-------------+
	| id | name        | description                       | price   | stock | category_id |
	+----+-------------+-----------------------------------+---------+-------+-------------+
	|  1 | sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
	|  2 | uniklooh    | baju keren dari brand ternam      |  500000 |    50 |           2 |
	|  3 | imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
	+----+-------------+-----------------------------------+---------+-------+-------------+
	3 rows in set (0.001 sec)

	
4	a. //memilih query pada tabel 
		MariaDB [myshop]> select id, name, email from users;
		+----+----------+--------------+
		| id | name     | email        |
		+----+----------+--------------+
		|  1 | john doe | john@doe.com |
		|  2 | jane doe | jane@doe.com |
		+----+----------+--------------+
		2 rows in set (0.000 sec)
		
	b. //mengambil data dengan > 1000000
		MariaDB [myshop]> select * from items where price > 1000000;
		+----+-------------+-----------------------------------+---------+-------+-------------+
		| id | name        | description                       | price   | stock | category_id |
		+----+-------------+-----------------------------------+---------+-------+-------------+
		|  1 | sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
		|  3 | imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
		+----+-------------+-----------------------------------+---------+-------+-------------+
		2 rows in set (0.002 sec)
		
		//mengambil data dengan kata kunci "watch"
		MariaDB [myshop]> select * from items where name like '%watch';
		+----+------------+-----------------------------------+---------+-------+-------------+
		| id | name       | description                       | price   | stock | category_id |
		+----+------------+-----------------------------------+---------+-------+-------------+
		|  3 | imho watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
		+----+------------+-----------------------------------+---------+-------+-------------+
		1 row in set (0.000 sec)
		
	c. //menampilkan data items  join dengan kategori
		MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.category_id, categories.name as categories from items inner join categories on items.category_id = categories.id;
		+-------------+-----------------------------------+---------+-------+-------------+------------+
		| name        | description                       | price   | stock | category_id | categories |
		+-------------+-----------------------------------+---------+-------+-------------+------------+
		| sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget     |
		| uniklooh    | baju keren dari brand ternam      |  500000 |    50 |           2 | cloth      |
		| imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget     |
		+-------------+-----------------------------------+---------+-------+-------------+------------+
		3 rows in set (0.003 sec
		

5.		//memperbarui data
		MariaDB [myshop]> update items set price = 2500000 where id = 1;
		Query OK, 1 row affected (0.015 sec)
		Rows matched: 1  Changed: 1  Warnings: 0
		
		menampilkan data yang telah diperbarui
		MariaDB [myshop]> select * from items;
		+----+-------------+-----------------------------------+---------+-------+-------------+
		| id | name        | description                       | price   | stock | category_id |
		+----+-------------+-----------------------------------+---------+-------+-------------+
		|  1 | sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
		|  2 | uniklooh    | baju keren dari brand ternam      |  500000 |    50 |           2 |
		|  3 | imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
		+----+-------------+-----------------------------------+---------+-------+-------------+
		3 rows in set (0.003 sec)